// var hostUrl = 'http://nodejs1.cbflifdpijvx.us-west-2.rds.amazonaws.com:3000/';
var hostUrl = 'http://localhost:3000/';

function deleteMe(obj, entity, id) {
    if (!confirm('Are you sure you want to delete this? It may lead to data loss!')) {
        window.location.href = '/show_' + entity + 's';
    }
    else {
        obj.href = '/delete_' + entity + '/' + id;
    }
}

function saveMe(obj, entity, id) {
    if (!confirm('Are you sure you want to delete this? It may lead to data loss!')) {
        window.location.href = '/show_' + entity + 's';
    }
    else {
        alert('i wanna delete');
        obj.href = '/delete_' + entity + '/' + id;
    }
}

function getTotal() {
    if (/^\d+$/.test(parseInt($("#fee").val()))) {
        $("#total").val(parseInt($("#fee").val()) + parseInt($("#dues").val()));
    }
    else {
        $("#total").val(parseInt($("#dues").val()));
    }
}


function getPatient() {
    var data = {};
    data['pid'] = $('select#patient_id').val();

    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: hostUrl + 'getDues',
        dataType: 'json',
        success: function (data) {
            console.log('returned data: ' + JSON.stringify(data));
            $('#dues').val( data['dues']) ;
            $('#total').val( data['dues']) ;
            // $('#no_of_session').val(data['no_of_session']) ;
        }
    });
}

function getSessions() {
    var data = {};
    data['pid'] = $('select#patient_id').val();
    $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        url: hostUrl + 'getSessions',
        dataType: 'json',
        success: function (data) {
            var arr = data['user_sessions_objs'];

            $('#session_table tr').remove();

            for (i in arr) {
                console.log(arr[i].id);
                // $("#session_table").append("<td>{{date}}</td>");
                var sid = arr[i].id;
                $('#session_table > tbody').after("<tr><td>" + arr[i].date + "</td><td>" + arr[i].session_number + "</td><td>" + arr[i].treatment_name + "</td><td>" + arr[i].strength_of_peel + "</td><td>" + arr[i].time + "</td><td>" + arr[i].meso_products + "</td><td>" + arr[i].special_remarks + "</td>" +
                    "<td>" +
                    "<a href='/edit_session/" + sid + "'  rel='tooltip' title='' class='btn btn-primary btn-simple btn-xs' data-original-title='Edit Session'> <i class='material-icons'>edit</i></a> " +
                    "<a href='' rel='tooltip' title='' class='btn btn-danger btn-simple btn-xs' data-original-title='Delete Session' onclick='deleteMe(this,'session',{{id}})'> <i class='material-icons'>close</i> </a></td></tr>"
                );
            }

        }
    });
}
