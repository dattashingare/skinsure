var module;
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mustacheExpress = require('mustache-express');
var stringify = require('node-stringify');
var session = require('express-session');
var app = express();
// 'use strict';
require('debug-fd-deprecated');

var doctor = require('./routes/admin');
var reception = require('./routes/reception');
var patient = require('./routes/patient');
var default_route = require('./routes/default');

app.use(session({
    secret: '1029384756qpwoeiruty',
    proxy: true,
    resave: true,
    saveUninitialized: true
}));

// view engine setup
app.set('views', path.join(__dirname, '/views'));

app.engine('mustache', mustacheExpress());
app.set('view engine', 'mustache');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// app.get('/test', patient.test);

app.get('/', default_route.home);
app.get('/home', default_route.home);
app.get('/logout', default_route.logout);

app.get('/signin', default_route.signin);
app.post('/authenticate_user', default_route.authenticate_user);

app.get('/register', doctor.register);
app.post('/register_user', doctor.register_user);
app.get('/edit_register_user/:id', doctor.edit_register_user);
app.post('/update_register_user', doctor.update_register_user);

// urls belongs to doctor

app.get('/show_users', doctor.show_users);
app.get('/delete_user/:id', doctor.delete_user);

app.get('/doctor_dashboard', doctor.doctor_dashboard);
app.get('/user_profile', doctor.user_profile);

app.get('/new_payment', doctor.new_payment);
app.post('/save_payment', doctor.save_payment);
app.get('/edit_payment/:id', doctor.edit_payment);
app.post('/update_payment', doctor.update_payment);
app.get('/show_payments', doctor.show_payments);
app.get('/delete_payment/:id', doctor.delete_payment);

app.post('/getDues', doctor.getDues);

// urls belongs to reception

app.get('/reception_dashboard', reception.reception_dashboard);


// urls belongs to doctor and reception

app.get('/new_medicine', reception.new_medicine);
app.post('/save_medicine', reception.save_medicine);
app.get('/edit_medicine/:id', reception.edit_medicine);
app.post('/update_medicine', reception.update_medicine);
app.get('/show_medicines', reception.show_medicines);
app.get('/delete_medicine/:id', reception.delete_medicine);

app.get('/new_treatment', reception.new_treatment);
app.post('/save_treatment', reception.save_treatment);
app.get('/edit_treatment/:id', reception.edit_treatment);
app.post('/update_treatment', reception.update_treatment);
app.get('/show_treatments', reception.show_treatments);
app.get('/delete_treatment/:id', reception.delete_treatment);


// urls belongs to patient

app.get('', patient.patient_dashboard);
app.get('/patient_dashboard', patient.patient_dashboard);
app.post('/search_patient', patient.search_patient);

app.get('/new_patient', patient.new_patient);
app.post('/save_patient', patient.save_patient);
app.post('/quick_save_patient', patient.quick_save_patient);
app.get('/edit_patient/:id', patient.edit_patient);
app.post('/update_patient', patient.update_patient);
app.get('/show_patients', patient.show_patients);
app.get('/patient_details/:id', patient.patient_details);
app.get('/delete_patient/:id', patient.delete_patient);

app.get('/new_appointment', patient.new_appointment);
app.get('/new_appointment/:id', patient.new_appointment);
app.post('/save_appointment', patient.save_appointment);
app.get('/edit_appointment/:id', patient.edit_appointment);
app.post('/update_appointment', patient.update_appointment);
app.get('/today_appointments', patient.today_appointments);
app.get('/check_appointments_this_week', patient.check_appointments_this_week);
app.get('/show_appointments', patient.show_appointments);
app.get('/delete_appointment/:id', patient.delete_appointment);

app.get('/new_prescription/:id', patient.new_prescription);
app.post('/save_prescription', patient.save_prescription);
app.get('/edit_prescription/:id', patient.edit_prescription);
app.post('/update_prescription', patient.update_prescription);
app.get('/show_prescriptions', patient.show_prescriptions);
app.get('/delete_prescription/:id', patient.delete_prescription);

app.get('/new_patient_treatment', patient.new_patient_treatment);
app.post('/save_patient_treatment', patient.save_patient_treatment);
app.get('/edit_patient_treatment/:id', patient.edit_patient_treatment);
app.post('/update_patient_treatment', patient.update_patient_treatment);
app.get('/show_patient_treatments', patient.show_patient_treatments);
app.get('/delete_patient_treatment/:id', patient.delete_patient_treatment);

app.get('/new_session', patient.new_session);
app.get('/new_session/:id', patient.new_session);
app.post('/save_session', patient.save_session);
app.get('/edit_session/:id', patient.edit_session);
app.post('/update_session', patient.update_session);
app.get('/show_sessions', patient.show_sessions);
app.get('/delete_session/:id', patient.delete_session);

app.get('/new_bloodtest', patient.new_bloodtest);
// app.get('/new_bloodtest/:id', patient.new_bloodtest);
// app.post('/save_bloodtest', patient.save_bloodtest);
// app.get('/edit_bloodtest/:id', patient.edit_bloodtest);
// app.post('/update_bloodtest', patient.update_bloodtest);
app.get('/show_bloodtests', patient.show_bloodtests);
// app.get('/delete_bloodtest/:id', patient.delete_bloodtest);


app.post('/getSessions', patient.getSessions);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
