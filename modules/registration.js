'use strict';
module.exports = function (sequelize, DataTypes) {
    var Registration = sequelize.define('Registration', {
            username: DataTypes.STRING,
            email: DataTypes.STRING,
            password: DataTypes.STRING
        }, {
            timestamps: false
        },
        {
            classMethods: {
                associate: function (models) {
                    // associations can be defined here
                }
            }
        });
    return Registration;
};