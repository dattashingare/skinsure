var express = require('express');
var mysql = require('mysql');
var router = express.Router();
var stringify = require('node-stringify');
var session = require('express-session');
var app = express();
var conObj = require('../skinsureDB');

var con = mysql.createConnection({
    host: conObj.host,
    user     : conObj.user,
    password : conObj.password,
    port     : conObj.port,
    database: conObj.database
});

app.use(session({
    secret: '1029384756qpwoeiruty',
    proxy: true,
    resave: true,
    saveUninitialized: true
}));

var sess;
var module;

module.exports = {

    whichUser: function (datadict) {
        if (sess.role === 'Doctor') {
            datadict['doctor'] = 'doctor';
        } else {
            if (sess.role === 'Reception') {
                datadict['reception'] = 'reception';
            }
        }
        return datadict;
    },

    reception_dashboard: function (req, res) {
        sess = req.session;

        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Reception dashboard';

            res.render('reception/reception_dashboard', datadict);
        }
        else {
            res.redirect('/signin');
        }
    },


    new_medicine: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Medicine';

            res.render('reception/medicine', datadict);
        }
        else {
            res.redirect('/signin');
        }
    },

    save_medicine: function (req, res) {

        sess = req.session;
        if (sess.email && sess.user_name) {

            var data = {
                name: req.body.name,
                type: req.body.type,
                strength: req.body.strength,
                unit: req.body.unit,
                instruction: req.body.instruction
            };

            qry = "INSERT INTO medicine SET ? ";

            con.query(qry, data, function (err, rows) {
                if (err) throw err;

                res.redirect('/new_medicine');

            });
        }
        else {
            res.redirect('/signin');
        }
    },

    edit_medicine: function (req, res, id) {

        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Medicine';


            con.query('select medicine.id,medicine.name,medicine.type as m_type,medicine.strength, medicine.unit as m_unit, medicine.instruction from medicine where id=' + req.param('id'), function (err, medicine) {
                if (err) throw err;
                // var jsonObj = {'m_id': req.param('id')};
                console.log(JSON.stringify(medicine));
                var x = JSON.stringify(medicine);
                datadict['medicine_obj'] = medicine;

                res.render('reception/medicine', datadict);
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    update_medicine: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {

            m_id = req.body.m_id;

            var values = {
                name: req.body.name,
                type: req.body.type,
                strength: req.body.strength,
                unit: req.body.unit,
                instruction: req.body.instruction
            };

            var qry = ('UPDATE medicine SET ? WHERE id = ?');
            // qry = 'UPDATE register SET ?, WHERE email = ?', [{user_name: req.body.user_name}, temp_email];

            con.query(qry, [values, m_id], function (err, rows) {
                if (err) throw err;
                // console.log(user);
                res.redirect('/show_medicines');
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    show_medicines: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Medicine list';

            con.query('select * from medicine', function (err, medicines) {
                if (err) throw err;

                datadict['medicine_objs'] = medicines;
                res.render('reception/show_medicines', datadict);
            });
        }
        else {
            res.redirect('/signin');
        }

    },

    delete_medicine: function (req, res, id) {
        sess = req.session;
        if (sess.email && sess.user_name) {

            var qry = ('delete from medicine WHERE id =' + req.param('id'));
            con.query(qry, function (err, rows) {
                if (err) throw  err;
                res.redirect('/show_medicines');
            });
        }
        else {
            res.redirect('/signin');
        }
    },


    new_treatment: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Treatment';

            res.render('reception/treatment', datadict);
        }
        else {
            res.redirect('/signin');
        }
    },

    save_treatment: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {

            var data = {
                name: req.body.name,
                // tag: req.body.tag,
                cost: req.body.cost,
                instruction: req.body.instruction
            };
            qry = "INSERT INTO treatment SET ? ";

            con.query(qry, data, function (err, treatment) {
                if (err) throw err;

                res.redirect('/new_treatment');
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    edit_treatment: function (req, res, id) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Treatment';

            con.query('select * from treatment where id =' + req.param('id'), function (err, treatment) {
                if (err) throw err;

                datadict['treatment_obj'] = treatment;
                res.render('reception/treatment', datadict);
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    update_treatment: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            t_id = req.body.t_id;

            var values = {
                name: req.body.name,
                // tag: req.body.tag,
                cost: req.body.cost,
                instruction: req.body.instruction
            };

            var qry = ('UPDATE treatment SET ? WHERE id = ?');

            con.query(qry, [values, t_id], function (err, rows) {
                if (err) throw err;

                res.redirect('/new_treatment');

            });
        }
        else {
            res.redirect('/signin');
        }
    },

    show_treatments: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Treatment list';

            con.query('select * from treatment', function (err, treatments) {
                if (err) throw err;

                datadict['treatment_objs'] = treatments;
                res.render('reception/show_treatments', datadict);
            });

        }
        else {
            res.redirect('/signin');
        }
    },

    delete_treatment: function (req, res, id) {
        sess = req.session;
        if (sess.email && sess.user_name) {

            var qry = ('delete from treatment WHERE id =' + req.param('id'));
            con.query(qry, function (err, rows) {
                if (err) throw  err;
                res.redirect('/show_treatments');
            });
        }
        else {
            res.redirect('/signin');
        }
    }

};

