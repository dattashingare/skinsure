var express = require('express');
var mysql = require('mysql');
var router = express.Router();
var conObj = require('../skinsureDB');

var con = mysql.createConnection({
    host: conObj.host,
    user: conObj.user,
    password: conObj.password,
    port: conObj.port,
    database: conObj.database
});

var module;

module.exports = {

    whichUser: function (datadict) {
        if (sess.role === 'Doctor') {
            datadict['doctor'] = 'doctor';
        } else {
            if (sess.role === 'Reception') {
                datadict['reception'] = 'reception';
            }
        }
        return datadict;
    },

    home: function (req, res, flag) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Skinsure Home';
            res.render('index', datadict);
        } else {
            res.redirect('/logout');
        }
    },

    err: function (req, res) {
        res.render('error');
    },

    test: function (req, res) {
        con.query('select * from patient', function (err, users) {
            if (err) throw err;
            res.render('doctor/doc_dashboard', {users_objs: users, title: 'Test'});
        });
    },

    signin: function (req, res) {
        res.render('signin', {title: 'Signin'});
    },

    logout: function (req, res) {
        req.session.destroy(function (err) {
            if (err) {
                console.log(err);
            } else {
                res.redirect('/signin');
            }
        })
    },

    authenticate_user: function (req, res) {
        con.query("select * from register where email=" + "'" + req.body.email + "'and password=" + "'" + req.body.password + "'", function (err, user) {
            console.error(err);
            console.debug(user);
            if (user.length < 1) {
                // invalid user or user details
                res.redirect('/signin');
            } else {
                sess = req.session;
                sess.email = user[0].email;
                sess.role = user[0].role;
                sess.user_name = user[0].user_name;

                res.redirect('/patient_dashboard');
            }
        });

    }

};
