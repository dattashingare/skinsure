var express = require('express');
var mysql = require('mysql');
var stringify = require('node-stringify');
var session = require('express-session');
// const Sequelize = require('sequelize');
const path = require('path');
// var x = require('../models');

var app = express();
var moment = require('moment');
var conObj = require('../skinsureDB');

var con = mysql.createConnection({
    host: conObj.host,
    user     : conObj.user,
    password : conObj.password,
    port     : conObj.port,
    database: conObj.database
});

app.use(session({
    secret: '1029384756qpwoeiruty',
    proxy: true,
    resave: true,
    saveUninitialized: true
}));

var sess;
var module;

module.exports = {

    whichUser: function (datadict) {
        if (sess.role === 'Doctor') {
            datadict['doctor'] = 'doctor';
        } else {
            if (sess.role === 'Reception') {
                datadict['reception'] = 'reception';
            }
        }
        return datadict;
    },

    patient_dashboard: function (req, res) {

        sess = req.session;

        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Patient dashboard';

            res.render('patient/patient_dashboard', datadict);
        }
        else {
            res.redirect('/signin');
        }
    },

    new_patient: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Patient';

            res.render('patient/patient', datadict);
        }
        else {
            res.redirect('/signin');
        }
    },

    save_patient: function (req, res) {
        sess = req.session;
        console.log(req.body.first_name);

        if (sess.email && sess.user_name) {
            var data = {

                profile_img: req.body.profile_img_str,
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                email: req.body.email,
                contact: req.body.contact,
                street: req.body.street,
                city: req.body.city,
                zipcode: req.body.zipcode,
                state: req.body.state,
                country: req.body.country,
                gender: req.body.gender,
                age: req.body.age,
                date_of_birth: req.body.date_of_birth,
                blood_group: req.body.blood_group,
                marital_status: req.body.marital_status,
                spouse_name: req.body.spouse_name,
                date_of_anniversary: req.body.date_of_anniversary,
                occupation: req.body.occupation,
                preferred_time_to_call: req.body.preferred_time_to_call,
                sms_updates: req.body.sms_updates,
                dermatologist_details: req.body.dermatologist_details,
                info_source: req.body.info_source
            };

            qry = "INSERT INTO patient SET ? ";

            con.query(qry, data, function (err, rows) {
                if (err) throw err;

                res.redirect('/show_patients');

            });
        }
        else {
            res.redirect('/signin');
        }
    },

    quick_save_patient: function (req, res) {
        datadict = {};
        datadict = module.exports.whichUser(datadict);
        datadict['title'] = 'Patient';

        sess = req.session;
        console.log(req.body.first_name);

        if (sess.email && sess.user_name) {
            var data = {

                profile_img: req.body.profile_img_str,
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                contact: req.body.contact,
                email: '',
                street: '',
                city: '',
                zipcode: '',
                state: '',
                country: '',
                gender: '',
                // age: '',
                date_of_birth: '',
                blood_group: '',
                marital_status: '',
                spouse_name: '',
                date_of_anniversary: '',
                occupation: '',
                preferred_time_to_call: '',
                sms_updates: '',
                dermatologist_details: '',
                info_source: ''
            };

            qry = "INSERT INTO patient SET ? ";

            con.query(qry, data, function (err, rows) {
                if (err) throw err;

                res.redirect('/show_patients');

            });
        }
        else {
            res.redirect('/signin');
        }
    },

    edit_patient: function (req, res, id) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Edit patient';

            // con.query('select * from patient where id=' + req.param('id'), function (err, patient) {
            con.query('select patient.profile_img,patient.id,patient.first_name,patient.last_name,patient.email,patient.contact,patient.street,patient.city,patient.zipcode,patient.state,patient.country,patient.gender, patient.age,patient.date_of_birth,patient.blood_group ,patient.marital_status,patient.spouse_name,patient.date_of_anniversary,patient.occupation, patient.preferred_time_to_call,patient.sms_updates,patient.dermatologist_details,patient.info_source  from patient where id=' + req.param('id'), function (err, patient) {
                // con.query('select patient.id,patient.first_name,patient.last_name,patient.email,patient.contact,patient.street,patient.city,patient.zipcode,patient.state,patient.country, patient.age,patient.date_of_birth,patient.blood_group as bgroup,patient.marital_status,patient.spouse_name,patient.date_of_anniversary,patient.occupation, patient.preferred_time_to_call,patient.sms_updates,patient.dermatologist_details,patient.info_source  from patient where id=' + req.param('id'), function (err, patient) {
                if (err) throw err;
                datadict['patient_obj'] = patient;

                res.render('patient/patient', datadict);
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    update_patient: function (req, res) {

        sess = req.session;
        if (sess.email && sess.user_name) {
            var values = {
                profile_img: req.body.profile_img_str,
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                email: req.body.email,
                contact: req.body.contact,
                street: req.body.street,
                city: req.body.city,
                zipcode: req.body.zipcode,
                state: req.body.state,
                country: req.body.country,
                gender: req.body.gender,
                age: req.body.age,
                date_of_birth: req.body.date_of_birth,
                blood_group: req.body.blood_group,
                marital_status: req.body.marital_status,
                spouse_name: req.body.spouse_name,
                date_of_anniversary: req.body.date_of_anniversary,
                occupation: req.body.occupation,
                preferred_time_to_call: req.body.preferred_time_to_call,
                sms_updates: req.body.sms_updates,
                dermatologist_details: req.body.dermatologist_details,
                info_source: req.body.info_source
            };

            var qry = ('UPDATE patient SET ? WHERE id = ?');

            con.query(qry, [values, req.body.p_id], function (err, rows) {

                if (err) throw err;
                res.redirect('/show_patients');

            });
        }
        else {
            res.redirect('/signin');
        }
    },

    show_patients: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Patient list';

            con.query('select * from patient ORDER BY first_name', function (err, users) {
                if (err) throw err;

                datadict['users_objs'] = users;
                res.render('patient/show_patients', datadict);
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    patient_details: function (req, res, id) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Patient details';

            con.query('select * from patient where id=' + req.param('id'), function (err, patient) {
                console.log(err);
                if (err) throw err;

                con.query('select * from appointment where patient_id=' + req.param('id'), function (err, appointments) {
                    console.log(err);
                    if (err) throw err;

                    con.query('select * from user_session where patient_id=' + req.param('id'), function (err, user_sessions) {
                        console.log(err);
                        if (err) throw err;

                        var qry = 'SELECT prescription.id,patient_prescription.id as patient_prescription_id,medicine.id as medicine_id,patient_prescription.instructions,patient_prescription.duration,patient_prescription.morning,patient_prescription.afternoon,patient_prescription.evening,medicine.name,medicine.strength,medicine.unit,medicine.type,medicine.instruction FROM prescription left JOIN patient_prescription ON  patient_prescription.prescription_id=prescription.id left JOIN medicine ON medicine.id =patient_prescription.medicine_id where patient_prescription.prescription_id=prescription.id and prescription.patient_id=' + req.param('id') + ' and prescription.date=(select max(prescription.date) from prescription where prescription.patient_id=' + req.param('id') + ')';
                        con.query(qry, function (err, prescriptions) {
                            if (err) throw err;

                            datadict['patient_obj'] = patient;
                            datadict['appointment_objs'] = appointments;
                            datadict['user_sessions_objs'] = user_sessions;
                            datadict['prescription_objs'] = prescriptions;
                            res.render('patient/patient_details', datadict);

                        });

                    });

                });
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    delete_patient: function (req, res, id) {
        sess = req.session;
        if (sess.email && sess.user_name) {

            var qry = ('delete from patient WHERE id =' + req.param('id'));

            con.query(qry, function (err, rows) {
                if (err) throw  err;
                res.redirect('/show_patients');
            });
        }
        else {
            res.redirect('/signin');
        }
    },


    new_patient_treatment: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Patient treatment';

            con.query('select * from patient', function (err, patients) {
                if (err) throw err;

                con.query('select * from treatment', function (err, treatments) {
                    if (err) throw err;

                    datadict['patient_objs'] = patients;
                    datadict['treatment_objs'] = treatments;
                    res.render('patient/patient_treatment', datadict);

                });
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    save_patient_treatment: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {

            var data = {
                patient_id: req.body.patient_id,
                date: moment(Date.now()).format('YYYY-MM-DD'),
                concern: req.body.concern,
                odp: req.body.odp,
                associated_features: req.body.associated_features,
                personal_history: req.body.personal_history,
                medical_history: req.body.medical_history,
                drug_history: req.body.drug_history,
                family_history: req.body.family_history,
                parlour_activities: req.body.parlour_activities,
                use_of_products: req.body.use_of_products,
                menstrual_history: req.body.menstrual_history,
                ocp_history: req.body.ocp_history,
                obstetrics_history: req.body.obstetrics_history,
                examination: req.body.examination,
                diagnosis: req.body.diagnosis,
                treatment_description: req.body.treatment_description,
                treatment_id: req.body.treatment_id,
                flag: req.body.flag
            };
            qry = "INSERT INTO patient_treatment SET ? ";

            con.query(qry, data, function (err, treatment) {
                if (err) throw err;

                res.redirect('/show_patient_treatments');

            });
        }
        else {
            res.redirect('/signin');
        }
    },

    edit_patient_treatment: function (req, res, id) {

        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Edit patient treatment';

            con.query('select * from patient_treatment where id =' + req.param('id'), function (err, patient_treatment_obj) {
                if (err) throw err;

                con.query('select * from treatment', function (err, treatments) {
                    con.query('select id, first_name, last_name from patient', function (err, patients) {

                        datadict['patient_treatment_obj'] = patient_treatment_obj;
                        datadict['patient_objs'] = patients;
                        datadict['treatment_objs'] = treatments;
                        res.render('patient/patient_treatment', datadict);
                    });
                });

            });
        }
        else {
            res.redirect('/signin');
        }
    },

    update_patient_treatment: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            t_id = req.body.t_id;

            var values = {
                date: moment(Date.now()).format('YYYY-MM-DD'),
                concern: req.body.concern,
                odp: req.body.odp,
                associated_features: req.body.associated_features,
                personal_history: req.body.personal_history,
                medical_history: req.body.medical_history,
                drug_history: req.body.drug_history,
                family_history: req.body.family_history,
                parlour_activities: req.body.parlour_activities,
                use_of_products: req.body.use_of_products,
                menstrual_history: req.body.menstrual_history,
                ocp_history: req.body.ocp_history,
                obstetrics_history: req.body.obstetrics_history,
                examination: req.body.examination,
                diagnosis: req.body.diagnosis,
                treatment_description: req.body.treatment_description,
                treatment_id: req.body.treatment_id,
                flag: req.body.flag
            };

            var qry = ('UPDATE patient_treatment SET ? WHERE id = ?');

            con.query(qry, [values, t_id], function (err, rows) {

                if (err) throw err;
                res.redirect('/show_treatments');

            });
        }
        else {
            res.redirect('/signin');
        }
    },

    show_patient_treatments: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Patient treatment list';

            con.query('select p.id,p.first_name,p.last_name, t.id, t.date, t.concern, t.odp, t.associated_features, t.personal_history, t.medical_history, t.drug_history, t.family_history, t.parlour_activities, t.use_of_products, t.menstrual_history, t.ocp_history, t.obstetrics_history, t.examination, t.diagnosis, t.treatment_description, t.treatment_id, t.flag from patient_treatment as t INNER JOIN patient as p ON t.patient_id = p.id  ', function (err, patient_treatments) {
                if (err) throw err;

                datadict['patient_treatment_objs'] = patient_treatments;
                res.render('patient/show_patient_treatments', datadict);
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    delete_patient_treatment: function (req, res, id) {
        sess = req.session;
        if (sess.email && sess.user_name) {

            var qry = ('delete from patient_treatment WHERE id =' + req.param('id'));

            con.query(qry, function (err, rows) {
                if (err) throw  err;

                res.redirect('/show_patient_treatments');
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    search_patient: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Patient search result';

            // var qry = "select * from patient where first_name like '%" + req.body.patient_name + "%' or last_name like '%" + req.body.patient_name + "%'";
            var qry = "select * from patient where first_name like '" + req.body.patient_name + "%'";

            con.query(qry, function (err, patients) {
                if (err) throw err;

                datadict['patient_objs'] = patients;
                res.render('patient/patient_search_result', datadict);
            });
        }
        else {
            res.redirect('/signin');
        }
    },


    new_appointment: function (req, res, id) {

        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Appointment';

            if (req.param('id')) {

                // var qry = 'select * from patient where id=?';
                con.query('select id, first_name, last_name from patient where id=' + req.param('id'), function (err, patients) {
                    if (err) throw err;

                    datadict['patient_objs'] = patients;
                    datadict['selected'] = 'selected';
                    res.render('patient/appointment', datadict);
                });

            }
            else {
                con.query('select * from patient', function (err, patients) {
                    if (err) throw err;

                    datadict['multiplePatients'] = 'true';
                    datadict['patient_objs'] = patients;
                    res.render('patient/appointment', datadict);
                });
            }
        }
        else {
            res.redirect('/signin');
        }
    },

    save_appointment: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {

            var data = {
                patient_id: req.body.patient_id,
                date: req.body.date,
                start_time: req.body.start_time,
                end_time: req.body.end_time,
                room: req.body.room,
                reason: req.body.reason,
                flag: req.body.flag
            };
            qry = "INSERT INTO appointment SET ? ";

            con.query(qry, data, function (err, rows) {
                if (err) throw err;
                res.redirect('/show_appointments');

            });
        }
        else {
            res.redirect('/signin');
        }
    },

    edit_appointment: function (req, res, id) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Edit appointment';

            con.query('select * from appointment where id=' + req.param('id'), function (err, appointment) {
                if (err) throw err;

                con.query('select * from patient where id=' + appointment[0].patient_id, function (err, patient) {
                    if (err) throw err;

                    datadict['patient_obj'] = patient;
                    datadict['appointment_obj'] = appointment;
                    res.render('patient/appointment', datadict);
                });
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    update_appointment: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {

            a_id = req.body.aid;

            var values = {
                patient_id: req.body.patient_id,
                date: req.body.date,
                start_time: req.body.start_time,
                end_time: req.body.end_time,
                room: req.body.room,
                reason: req.body.reason,
                flag: req.body.flag
            };

            var qry = ('UPDATE appointment SET ? WHERE id = ?');
            // qry = 'UPDATE register SET ?, WHERE email = ?', [{user_name: req.body.user_name}, temp_email];

            con.query(qry, [values, a_id], function (err, rows) {
                if (err) throw err;

                res.redirect('/show_appointments');
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    today_appointments: function (req, res) {
        qry = "select * from appointment where date=?";

        var today = (moment(Date.now()).format('YYYY-MM-DD'));
        con.query(qry, today, function (err, user) {
            if (err) throw err;
            res.redirect('/home');
        });
    },

    check_appointments_this_week: function (req, res) {

        // qry = "select * from appointment where date between ? and ?";
        qry = 'select appointment.id,appointment.date,appointment.room,appointment.start_time,appointment.end_time,appointment.reason,appointment.flag,patient.first_name,patient.last_name,patient.contact  from appointment join patient on patient.id =appointment.patient_id where date between ? and ?';
        dt = moment(Date.now()).format('YYYY-MM-DD');

        var date = moment().add(7, 'days').toDate();
        con.query(qry, [dt, date], function (err, apps) {
            res.redirect('/home');

        });
    },

    show_appointments: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Appointment list';

            var today = (moment(Date.now()).format('YYYY-MM-DD'));

            var today_qry = 'select appointment.id,appointment.date,appointment.room,appointment.start_time,appointment.end_time,appointment.reason,appointment.flag,patient.first_name, patient.last_name from appointment join patient on patient.id =appointment.patient_id where appointment.date=?';
            // con.query('select appointment.id,appointment.date,appointment.room,appointment.start_time,appointment.end_time,appointment.reason,appointment.flag,patient.first_name, patient.last_name from appointment join patient on patient.id =appointment.patient_id', function (err, appointments) {

            con.query(today_qry, today, function (err, todays_objs) {
                weekly_qry = "SELECT appointment.id,appointment.date,appointment.room,appointment.start_time,appointment.end_time,appointment.reason,appointment.flag,patient.first_name, patient.last_name from appointment join patient on patient.id =appointment.patient_id WHERE WEEKOFYEAR(appointment.date)=WEEKOFYEAR(NOW())";

                // weekly_qry = 'select appointment.id,appointment.date,appointment.room,appointment.start_time,appointment.end_time,appointment.reason,appointment.flag,patient.first_name,patient.last_name,patient.contact  from appointment join patient on patient.id =appointment.patient_id where date between ? and ?';
                // var today_date = moment(Date.now()).format('YYYY-MM-DD');
                // var next_date = moment().add(7, 'days').toDate();

                con.query(weekly_qry, function (err, weekly_objs) {

                    month_qry = "SELECT appointment.id,appointment.date,appointment.room,appointment.start_time,appointment.end_time,appointment.reason,appointment.flag,patient.first_name, patient.last_name from appointment join patient on patient.id =appointment.patient_id WHERE YEAR(appointment.date) = YEAR(CURRENT_DATE()) AND MONTH(appointment.date) = MONTH(CURRENT_DATE())";
                    con.query(month_qry, function (err, month_objs) {


                        next_month_qry = "SELECT appointment.id,appointment.date,appointment.room,appointment.start_time,appointment.end_time,appointment.reason,appointment.flag,patient.first_name, patient.last_name from appointment join patient on patient.id =appointment.patient_id WHERE appointment.date= date_sub(CURRENT_DATE(),interval 1 month)";
                        con.query(next_month_qry, function (err, next_month_objs) {

                            datadict['todays_objs'] = todays_objs;
                            datadict['weekly_objs'] = weekly_objs;
                            datadict['month_objs'] = month_objs;
                            datadict['next_month_objs'] = next_month_objs;
                            res.render('patient/show_appointments', datadict);
                        });
                    });
                });
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    delete_appointment: function (req, res, id) {
        sess = req.session;
        if (sess.email && sess.user_name) {

            var qry = ('delete from appointment WHERE id =' + req.param('id'));

            con.query(qry, function (err, rows) {
                if (err) throw  err;
                res.redirect('/show_appointments');
            });
        }
        else {
            res.redirect('/signin');
        }
    },


    new_prescription: function (req, res, id) {
        sess = req.session;
        // if (1==1) {
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Prescription';

            con.query('select * from patient where id=' + req.param('id'), function (err, patient) {
                // con.query('select * from patient wehre id='+req.param('id'), function (err, patients) {
                if (err) throw err;
                con.query('select * from medicine', function (err, medicines) {

                    datadict['patients_obj'] = patient;
                    datadict['medicine_objs'] = medicines;
                    res.render('patient/prescription', datadict);
                });

            });
        }
        else {
            res.redirect('/signin');
        }

    },

    save_prescription: function (req, res) {
        sess = req.session;
        // if (1==1) {
        if (sess.email && sess.user_name) {


            var data = {
                patient_id: req.body.patient_id,
                treatment_name: req.body.treatment_name,
                // date: new Date()
                date: "2017-05-16"
            };

            var data1 = {
                medicine_id: req.body.medicine_id,
                morning: req.body.morning_medicine,
                afternoon: req.body.afternoon_medicine,
                evening: req.body.evening_medicine,
                duration: "1",
                instructions: req.body.instruction
            };

            qry = "INSERT INTO prescription SET ? ";
            qry1 = "INSERT INTO patient_prescription SET ? ";

            con.query(qry, data, function (err, prescription_obj) {

                con.query(qry1, data1, function (err, patient_prescription_objs) {
                    if (err) throw err;
                    res.redirect('/show_prescriptions');

                });
            });

        }
        else {
            res.redirect('/signin');
        }
    },

    edit_prescription: function (req, res, id) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Edit prescription';

            pid = req.param('id');
            con.query('select * from prescription where id=' + req.param('id'), function (err, prescription) {
                if (err) throw err;
                con.query('select * from patient', function (err, patients) {
                    if (err) throw err;

                    datadict['patients_objs'] = patients;
                    datadict['prescription_obj'] = prescription;
                    datadict['p_id'] = pid;
                    res.render('patient/prescription', datadict);
                });
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    update_prescription: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {

            p_id = req.body.p_id;

            var values = {
                patient_id: req.body.patient_id,
                date: req.body.date,
                start_time: req.body.start_time,
                end_time: req.body.end_time,
                room: req.body.room,
                reason: req.body.reason,
                flag: req.body.flag
            };

            var qry = ('UPDATE prescription SET ? WHERE id = ?');
            // qry = 'UPDATE register SET ?, WHERE email = ?', [{user_name: req.body.user_name}, temp_email];

            con.query(qry, [values, f_id], function (err, prescription) {
                if (err) throw err;

                res.redirect('/show_prescriptions');
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    show_prescriptions: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Prescription list';

            con.query('select appointment.id,appointment.date,appointment.room,appointment.start_time,appointment.end_time,appointment.reason,appointment.flag,patient.first_name, patient.last_name from appointment join patient on patient.id =appointment.patient_id', function (err, prescriptions) {
                // con.query('select * from appointment', function (err, appointments) {
                if (err) throw err;

                datadict['prescription_objs'] = prescriptions;
                res.render('patient/prescription', datadict);
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    delete_prescription: function (req, res, id) {
        sess = req.session;
        if (sess.email && sess.user_name) {

            var qry = ('delete from prescription WHERE id =' + req.param('id'));

            con.query(qry, function (err, prescription) {
                if (err) throw  err;

                res.redirect('/show_prescriptions');
            });
        }
        else {
            res.redirect('/signin');
        }
    },


    new_session: function (req, res, id) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'User session';

            if (req.param('id')) {

                con.query('select id,name from treatment', function (err, treatments) {
                    if (err) throw err;
                    // con.query('select * from user_session where patient_id=' + 2, function (err, user_sessions) {
                    con.query('select * from user_session where patient_id=' + req.param('id'), function (err, user_sessions) {
                        if (err) throw err;

                        con.query('select id, first_name, last_name from patient where id=' + req.param('id'), function (err, patients) {
                            if (err) throw err;

                            datadict['treatment_objs'] = treatments;
                            datadict['patient_objs'] = patients;
                            datadict['user_sessions_objs'] = user_sessions;
                            res.render('patient/session', datadict);
                        });
                    });

                });
            }
            else {
                con.query('select id,name from treatment', function (err, treatments) {
                    if (err) throw err;
                    // con.query('select * from user_session where patient_id=' + 2, function (err, user_sessions) {
                    //     // con.query('select * from user_session where patient_id=' + req.param('id'), function (err, user_sessions) {
                    //     if (err) throw err;

                    con.query('select id,first_name,last_name from patient', function (err, patients) {
                        if (err) throw err;

                        datadict['treatment_objs'] = treatments;
                        datadict['patient_objs'] = patients;
                        // datadict['user_sessions_objs'] =user_sessions;
                        res.render('patient/session', datadict);
                    });
                });

                // });

            }

        }
        else {
            res.redirect('/signin');
        }
    },

    save_session: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {

            var data = {

                patient_id: req.body.patient_id,
                treatment_name: req.body.treatment_name,
                session_number: req.body.session_number,
                strength_of_peel: req.body.strength_of_peel,
                time: req.body.time,
                date: req.body.date,
                meso_products: req.body.meso_products,
                special_remarks: req.body.special_remarks,
                treatment_img: req.body.treatment_img_str
            };
            qry = "INSERT INTO user_session SET ? ";

            con.query(qry, data, function (err, rows) {
                if (err) throw err;
                res.redirect('/new_session');

            });
        }
        else {
            res.redirect('/signin');
        }
    },

    edit_session: function (req, res, id) {

        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Edit session';

            // var qry = "select patient_id, treatment_name, session_number, strength_of_peel, time, meso_products, special_remarks from user_session, where id=" + req.param('id');
            var qry = 'select * from user_session where id=' + req.param('id');
            con.query(qry, function (err, user_session) {
                // con.query(qry, function (err, user_session) {
                if (err) throw err;

                con.query('select id,name from treatment', function (err, treatments) {
                    if (err) throw err;

                    con.query('select id,first_name,last_name from patient', function (err, patients) {
                        if (err) throw err;

                        con.query('select * from user_session where patient_id=' + 2, function (err, user_sessions) {
                            // con.query('select * from user_session where patient_id=' + req.param('id'), function (err, user_sessions) {
                            if (err) throw err;

                            datadict['session_obj'] = user_session;
                            datadict['user_sessions_objs'] = user_sessions;
                            datadict['patient_objs'] = patients;
                            datadict['treatment_objs'] = treatments;
                            res.render('patient/session', datadict);
                        });
                    });
                });
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    update_session: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            s_id = req.body.s_id;

            var values = {
                patient_id: req.body.patient_id,
                treatment_name: req.body.treatment_name,
                session_number: req.body.session_number,
                strength_of_peel: req.body.strength_of_peel,
                time: req.body.time,
                meso_products: req.body.meso_products,
                special_remarks: req.body.special_remarks
            };

            con.query('UPDATE user_session SET ? WHERE id = ?', [values, s_id], function (err, rows) {

                if (err) throw err;
                res.redirect('/new_session');

            });
        }
        else {
            res.redirect('/signin');
        }
    },

    show_sessions: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Session list';

            qry = 'SELECT user_session.id as user_session, user_session.treatment_img as treatment_img,patient.id as patient_id,patient.first_name,patient.last_name,user_session.treatment_name ,user_session.session_number,user_session.date,user_session.strength_of_peel,user_session.time,user_session.special_remarks,user_session.meso_products FROM user_session INNER JOIN patient ON patient.id =user_session.patient_id';
            con.query(qry, function (err, user_sessions) {
                if (err) throw err;

                datadict['session_objs'] = user_sessions;
                res.render('patient/show_sessions', datadict);
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    delete_session: function (req, res, id) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            var qry = ('delete from user_session WHERE id =' + req.param('id'));

            con.query(qry, function (err, rows) {
                if (err) throw  err;

                res.redirect('/show_sessions');
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    new_bloodtest: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Blood test';

            res.render('patient/blood_test', datadict);

            // con.query('select id,name from bloodtest', function (err, bloodtest) {
            //     if (err) throw err;
            //
            //     con.query('select id,first_name,last_name from patient', function (err, patients) {
            //         if (err) throw err;
            //         res.render('patient/blood_test', {
            //             title: 'Blood test From'
            //         });
            //     });
            //
            // });
        }
        else {
            res.redirect('/signin');
        }
    },

    show_bloodtests: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Blood test list';

            res.render('patient/show_bloodtests', datadict);
        }
        else {
            res.redirect('/signin');
        }
    },


    getSessions: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            var qry = ('select * from user_session WHERE patient_id =' + req.body["pid"]);

            con.query(qry, function (err, session_objs) {
                if (err) throw  err;
                if (session_objs.length > 0) {
                    var obj = {
                        'user_sessions_objs': session_objs
                    }
                } else {
                    var obj = {
                        'user_sessions_objs': ''
                    };
                }
                res.send(obj);

            });
        }
        else {
            res.redirect('/signin');
        }
    }

};
