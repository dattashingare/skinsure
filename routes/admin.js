var express = require('express');
var mysql = require('mysql');
var router = express.Router();
var stringify = require('node-stringify');
var session = require('express-session');
var app = express();

var moment = require('moment');
var conObj = require('../skinsureDB');

var con = mysql.createConnection({
    host: conObj.host,
    user     : conObj.user,
    password : conObj.password,
    port     : conObj.port,
    database: conObj.database
});

app.use(session({
    secret: '1029384756qpwoeiruty',
    proxy: true,
    resave: true,
    saveUninitialized: true
}));

var sess;
var module;

module.exports = {

    whichUser: function (datadict) {
        if (sess.role === 'Doctor') {
            datadict['doctor'] = 'doctor';
        } else {
            if (sess.role === 'Reception') {
                datadict['reception'] = 'reception';
            }
        }
        return datadict;
    },

    register: function (req, res) {
        res.render('register', {title: 'Register'});
    },

    register_user: function (req, res) {

        var data = {
            user_name: req.body.user_name,
            email: req.body.email,
            contact: req.body.contact,
            password: req.body.password,
            role: req.body.role
        };

        qry = "INSERT INTO register SET ? ";

        con.query(qry, data, function (err, user) {
            if (err) throw  err;

            sess = req.session;
            sess.email = req.body.email;
            sess.user_name = req.body.user_name;
            res.redirect('/home');
        });
    },

    edit_register_user: function (req, res, id) {

        if (sess.email && sess.user_name) {
            con.query('select * from register where id=' + req.param('id'),
                function (err, user) {
                    if (err) throw err;

                    res.render('register', {user_obj: user, title: 'Register'});
                });
        }
        else {
            res.redirect('/logout');
        }

    },

    update_register_user: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {

            temp_email = req.body.email;

            var values = {
                user_name: req.body.user_name,
                email: req.body.email,
                contact: req.body.contact,
                password: req.body.password,
                role: req.body.role
            };

            var qry = ('UPDATE register SET ? WHERE email = ?');
            // qry = 'UPDATE register SET ?, WHERE email = ?', [{user_name: req.body.user_name}, temp_email];

            con.query(qry, [values, req.body.email], function (err, rows) {
                if (err) throw err;
                // console.log(user);

                res.redirect('/user_profile');
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    show_users: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'User list';

            con.query('select * from register', function (err, users) {
                if (err) throw err;

                datadict['users_objs'] = users;
                res.render('doctor/show_users', datadict);
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    delete_user: function (req, res, id) {
        sess = req.session;
        if (sess.email && sess.user_name) {

            var qry = ('delete from register WHERE id =' + req.param('id'));

            con.query(qry, function (err, rows) {
                if (err) throw  err;
                console.log("Error inserting : %s ", err);
                res.redirect('/show_users');
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    doctor_dashboard: function (req, res) {

        sess = req.session;

        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Doctor dashboard';

            res.render('doctor/doctor_dashboard', datadict);
        }
        else {
            res.redirect('/signin');
        }
    },

    user_profile: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'User profile';

            con.query("select register.user_name,register.email,register.contact,register.role from register where email=" + "'" + sess.email + "'", function (err, user) {
                datadict['user_obj'] = user;
                res.render('doctor/user_profile', datadict);
            });
        }
        else {
            res.redirect('/logout');
        }


    },


    new_payment: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Payment';

            con.query('select * from patient', function (err, patients) {
                if (err) throw err;

                datadict['multiplePatients'] = 'true';
                datadict['patient_objs'] = patients;
                res.render('doctor/payment', datadict);
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    save_payment: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            var data = {
                patient_id: req.body.patient_id,
                date: moment(Date.now()).format('YYYY-MM-DD'),
                service_type: req.body.service_type,
                fee: req.body.fee,
                dues: req.body.dues,
                total: req.body.total,
                no_of_session: req.body.no_of_session,
                remark: req.body.remark
            };

            var qry = "INSERT INTO payment SET ? ";

            con.query(qry, data, function (err, payments) {
                if (err) throw err;

                res.redirect('/new_payment');
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    edit_payment: function (req, res, id) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Edit payments';

            pid = req.param('id');
            con.query('select * from payment where id=' + req.param('id'), function (err, payments) {
                if (err) throw err;
                con.query('select * from patient', function (err, patients) {
                    if (err) throw err;

                    datadict['p_id'] = pid;
                    datadict['patients_objs'] = patients;
                    datadict['payment_obj'] = payments;

                    res.render('doctor/payment', datadict);
                });
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    update_payment: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {

            p_id = req.body.p_id;

            var values = {
                patient_id: req.body.patient_id,
                date: req.body.date,
                start_time: req.body.start_time,
                end_time: req.body.end_time,
                room: req.body.room,
                reason: req.body.reason,
                flag: req.body.flag
            };

            var qry = ('UPDATE payment SET ? WHERE id = ?');
            // qry = 'UPDATE register SET ?, WHERE email = ?', [{user_name: req.body.user_name}, temp_email];

            con.query(qry, [values, p_id], function (err, payments) {
                if (err) throw err;
                // console.log(user);

                res.redirect('/new_payment');
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    show_payments: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            datadict = {};
            datadict = module.exports.whichUser(datadict);
            datadict['title'] = 'Payment list';

            // con.query('select * from payment', function (err, payments) {
            con.query('select pa.first_name,pa.last_name, py.id, py.date, py.service_type, py.fee, py.dues, py.total, py.no_of_session, py.remark from payment as py INNER JOIN patient as pa ON py.patient_id = pa.id  ', function (err, payments) {
                // con.query('select * from appointment', function (err, appointments) {
                if (err) throw err;

                datadict['payment_objs'] = payments;
                res.render('doctor/show_payments', datadict);
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    delete_payment: function (req, res, id) {
        sess = req.session;
        if (sess.email && sess.user_name) {

            var qry = ('delete from payment WHERE id =' + req.param('id'));

            con.query(qry, function (err, payment) {
                if (err) throw  err;
                console.log("Error inserting : %s ", err);
                res.redirect('/show_payments');
            });
        }
        else {
            res.redirect('/signin');
        }
    },

    getDues: function (req, res) {
        sess = req.session;
        if (sess.email && sess.user_name) {
            var qry = ('select * from payment WHERE patient_id =' + req.body["pid"]);

            con.query(qry, function (err, payment) {
                if (err) throw  err;
                if (payment.length > 0) {
                    var obj = {
                        'patient_id': payment[0].patient_id,
                        'dues': payment[0].dues,
                        // 'no_of_session': parseInt(payment[0].no_of_session) + 1
                    }
                } else {
                    var obj = {
                        'dues': 0,
                        // 'no_of_session': 1
                    };
                }
                res.send(obj);

            });
        }
        else {
            res.redirect('/signin');
        }
    }

};